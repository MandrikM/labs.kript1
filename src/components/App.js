import React, {Component} from 'react'
import Tabl from './Tabl'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {createArr} from './actions'


class App extends Component{


  render(){
  
        
    return (
      <div>
      <button  onClick= {() => {this.props.createArr(5,5)}} >
          show table
        </button>
      
      <Tabl />

      </div>
      )
  }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({createArr: createArr},dispatch)
}

export default connect(null, mapDispatchToProps )(App)

 