import React from "react";
import ReactDOM from "react-dom";



let data = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]]
let mData = [[0.1,0.2,0.1,0.2,0.1,0.3]]
let kData = [[0.5,0.1,0.1,0.1,0.1,0.1]]
let MessageCount = data[0].length
let KeyCount = data.length

function Cell(props) {
      return (
        <input
        type="number"
        defaultValue={props.myTable[props.row][props.col]}
        onChange={(e) => {
          props.myTable[props.row][props.col] = e.target.value
        }}/>
        )
  }

function ShowTable(props) {
	return(
		<table>
	      <tbody>
	      {
	        props.myTable.map((row, i) => (
	        <tr>
	        {row.map((_, j) => (<td><Cell row={i} col={j} myTable={props.myTable}/></td>))}
	        </tr>)
	      )
	    }
	    </tbody>
	    </table>
	)
}

function Fi (m,k,e){
	if (data[k][m]=== e) {return 1} else {return 0}
}

function Pem(m, e){
	return mData[0][m]*Pme(m ,e )/Pe(e)
}

function Pek(k,e){
	return kData[0][k]* Pke(k, e)/ Pe(e)
}

function Pme( m, e){
	let sum = 0
	for (var k = 0; k < kData[0].length; k++) {
		sum += kData[0][k] * Fi(m,k,e)
	}
	return sum
}

function Pe(e){
	let sum =0
	for (var m = 0; m < MessageCount; m++) {
		for (var k = 0; k < KeyCount; k++) {
			sum += kData[0][k]* mData[0][m]* Fi(m,k,e)
		}
	}
	return sum
}

function Pke(k, e){
	let sum = 0
	for (var m = 0; m < MessageCount; m++) {
		sum += mData[0][m]* Fi(m,k,e)
	}
	return sum
}

function PmeTable(myTable, func){
	for (var i = 0; i < myTable.length; i++) {
		for (var j = 0; j < myTable[i].length; j++) {
			myTable[i][j]= func(i,j)
		}
	}
}

function App() {
  return (
    <div >
    P table
      <ShowTable myTable = {data} />
      <hr />
      M table
      <ShowTable myTable = {mData} />
      <hr />
      K table
      <ShowTable myTable = {kData} />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);